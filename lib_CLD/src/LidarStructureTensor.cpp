#include "../LidarClassifStuff/LidarStructureTensor.h"
//#include "../LidarClassifStuff/PCA/EigenMatrix.h"
//#include "LfTools/Dimensionality/Dimensionality.h"

#include "../LidarClassifStuff/List/ListDimensionality.h"
#include "../LidarClassifStuff/Signature.h"

void LidarStructureTensor::runMultiscaleEa()
{
    for(
            m_listCoord.itPoint() = m_listCoord.Begin(),
            m_listKNearestNeighbors.itPoint() = m_listKNearestNeighbors.Begin(),
            m_listNeighborBounds.itPoint() = m_listNeighborBounds.Begin(),
            m_listStructureTensor.itPoint() = m_listStructureTensor.Begin(),
            m_listDimensionality.itPoint() = m_listDimensionality.Begin();

    m_listCoord.itPoint() != m_listCoord.End();

    ++ m_listCoord.itPoint(),
    ++ m_listKNearestNeighbors.itPoint(),
    ++ m_listNeighborBounds.itPoint(),
    ++ m_listStructureTensor.itPoint(),
    ++ m_listDimensionality.itPoint()
    )
    {
        multiscaleEa();
    }
}

void LidarStructureTensor::runSimilarity()
{
    for(
            m_listCoord.itPoint() = m_listCoord.Begin(),
            //m_listKNearestNeighbors.itPoint() = m_listKNearestNeighbors.Begin(),
            m_listDimensionality.itPoint() = m_listDimensionality.Begin();

    m_listCoord.itPoint() != m_listCoord.End();

    ++ m_listCoord.itPoint(),
    ++ m_listKNearestNeighbors.itPoint(),
    ++ m_listDimensionality.itPoint()
    )
    {
        similarity();
    }
}

void LidarStructureTensor::multiscaleEa()
{
    const int kMin_ = m_listNeighborBounds.kMin.Value(m_listNeighborBounds.itPoint());
    const int kMax_ = m_listNeighborBounds.kMax.Value(m_listNeighborBounds.itPoint());

    const int kMin = std::min(kMin_,kMax_);
    const int kMax = kMax_;

    static StructureTensor structure_tensor;
    structure_tensor.Clear();

    float min_entropy_value =1;

    float max_D1_value =0;
    float max_D2_value =0;
    float max_D3_value =0;

    float max_D1_entropy =1;
    float max_D2_entropy =1;
    float max_D3_entropy =1;

    int max_D1_k =0;
    int max_D2_k =0;
    int max_D3_k =0;

    int optimal_k = 0;
    //EigenMatrix< double > eigenMatrix;
    StructDimensionality dimensionality;

    int nb_labellings = 0;
    int Dmax = 0;
    Signature signature;


    const int kBegin = 0;
    const int kEnd   = std::min<int>(kMax+1,m_listKNearestNeighbors.neighbor.size());
    for (int ki=kBegin; ki!=kEnd; ++ki)
    {
        const unsigned int indexVoisin = m_listKNearestNeighbors.index(m_listKNearestNeighbors.itPoint(), ki);
        Lidar::LidarDataContainer::iterator itVoisinCoord =  m_listCoord.Begin() + indexVoisin;

        structure_tensor(
                m_listCoord.x.Value(itVoisinCoord),
                m_listCoord.y.Value(itVoisinCoord),
                m_listCoord.z.Value(itVoisinCoord)
                );


        if( (ki>=kMin)&&(ki>=4) )
        {
            structure_tensor.FindEigen();
            //SpatialAttributes spatialAttributes = structure_tensor.GetSpatialAttributes();
            //spatialAttributes.normalizeByLambda1();

            const float entropy_value = structure_tensor.GetDim().Entropy();
            if(entropy_value<min_entropy_value)
            {

                min_entropy_value = entropy_value;
                optimal_k = ki;
                //eigenMatrix = structure_tensor.GetEigenMatrix();
                dimensionality.D1      = structure_tensor.GetDim().D1();
                dimensionality.D2      = structure_tensor.GetDim().D2();
                dimensionality.D3      = structure_tensor.GetDim().D3();
                dimensionality.entropy = entropy_value;
                dimensionality.Dmax    = structure_tensor.GetDim().DMax();
            }

            if(structure_tensor.GetDim().D1()>max_D1_value)
            {
                max_D1_value = structure_tensor.GetDim().D1();
                max_D1_entropy = entropy_value;
                max_D1_k = ki;
            }
            if(structure_tensor.GetDim().D1()>max_D2_value)
            {
                max_D2_value = structure_tensor.GetDim().D2();
                max_D2_entropy = entropy_value;
                max_D2_k = ki;
            }
            if(structure_tensor.GetDim().D3()>max_D3_value)
            {
                max_D3_value = structure_tensor.GetDim().D3();
                max_D3_entropy = entropy_value;
                max_D3_k = ki;
            }
            if(structure_tensor.GetDim().DMax()!=Dmax)
            {
                Dmax = structure_tensor.GetDim().DMax();
                ++nb_labellings;
            }

            //signature(Dmax, ki);
        }
    }

    StructStructureTensor structure = structure_tensor.GetStruct();

    m_listStructureTensor.Set(m_listCoord.itPoint(), structure);
    m_listDimensionality.Set(m_listCoord.itPoint(), dimensionality);

    m_listDimensionality.k.Value(m_listDimensionality.itPoint()) = optimal_k;
    m_listDimensionality.radius.Value(m_listDimensionality.itPoint()) = std::min(distance(m_listKNearestNeighbors.itPoint(), optimal_k), m_listNeighborBounds.radiusMax.Value(m_listNeighborBounds.itPoint()));

    //m_listScales.entropy_Dmax.Value(m_listScales.itPoint()) = dimensionality.Dmax;
    //m_listScales.entropy_entropy.Value(m_listScales.itPoint()) = min_entropy_value;
    //m_listScales.entropy_k.Value(m_listScales.itPoint()) = optimal_k;
    //m_listScales.entropy_radius.Value(m_listScales.itPoint()) = distance(optimal_k);


    //m_listScales.k_D1_max.Value(m_listScales.itPoint()) = max_D1_k;
    //m_listScales.k_D2_max.Value(m_listScales.itPoint()) = max_D2_k;
    //m_listScales.k_D3_max.Value(m_listScales.itPoint()) = max_D3_k;

    m_listDimensionality.nb_labellings.Value(m_listCoord.itPoint()) =nb_labellings;
    //m_listDimensionality.nb_labellings.Value(m_listCoord.itPoint()) =signature.Get();
}

void LidarStructureTensor::similarity()
{
    int optimal_k = m_listDimensionality.k.Value(m_listDimensionality.itPoint());
    int my_Dmax = m_listDimensionality.Dmax.Value(m_listDimensionality.itPoint());
    int nb_like_me =0;

    int nb_D1 =0;
    int nb_D2 =0;
    int nb_D3 =0;

    const int kBegin = 0;
    const int kEnd   = optimal_k+1;
    for (int ki=kBegin; ki!=kEnd; ++ki)
    {
        const unsigned int indexVoisin = m_listKNearestNeighbors.index(m_listKNearestNeighbors.itPoint(), ki);
        Lidar::LidarDataContainer::iterator itVoisinScale=  m_listDimensionality.Begin() + indexVoisin;
        int voisin_Dmax = m_listDimensionality.Dmax.Value(itVoisinScale);
        if(voisin_Dmax==my_Dmax) ++nb_like_me;

        switch(voisin_Dmax)
        {
        case 1 : ++nb_D1; break;
        case 2 : ++nb_D2; break;
        case 3 : ++nb_D3; break;
        }
    }

    //(nb_like_me-1) car le point (voisin d index 0) est forcement classe comme lui meme
    m_listDimensionality.similarity.Value(m_listDimensionality.itPoint()) = (float) (nb_like_me-1) / (float) optimal_k;

    if((nb_D1 > nb_D2) && (nb_D1 > nb_D3))
    {
        m_listDimensionality.Dmax_of_neighbors.Value(m_listDimensionality.itPoint()) = 1;
    }
    else if( nb_D2 > nb_D3 )
    {
        m_listDimensionality.Dmax_of_neighbors.Value(m_listDimensionality.itPoint()) = 2;
    }
    else
    {
        m_listDimensionality.Dmax_of_neighbors.Value(m_listDimensionality.itPoint()) = 3;
    }
}


// trouver la distance du ke voisin au point
const float LidarStructureTensor::distance(Lidar::LidarDataContainer::iterator it_point, const int k)
{
    const unsigned int index = m_listKNearestNeighbors.index(it_point, k);
    Lidar::LidarDataContainer::iterator it =  m_listCoord.Begin() + (index);
    return m_listCoord.Distance(m_listCoord.itPoint(),it);
}

int LidarStructureTensor::order(const float radiusD1, const float radiusD2, const float radiusD3)
{
    if( (radiusD1>=radiusD2) && (radiusD1>=radiusD3) )
    {
        if(radiusD2 >= radiusD3 ) return 0;
        return 1;
    }
    if( (radiusD2>=radiusD1) && (radiusD2>=radiusD3) )
    {
        if(radiusD1 >= radiusD3 ) return 2;
        return 3;
    }
    if(radiusD1 >= radiusD2 ) return 4;
    return 5;
}

void runGlobalDim(ListCoord& listCoord)
{
    static StructureTensor structure_tensor;
    structure_tensor.Clear();
    //EigenMatrix< double > eigenMatrix;
    StructDimensionality dimensionality;

    for(
            listCoord.itPoint() = listCoord.Begin();
    listCoord.itPoint() != listCoord.End();
    ++ listCoord.itPoint()

    )
    {
        structure_tensor(
                listCoord.x.Value(listCoord.itPoint()),
                listCoord.y.Value(listCoord.itPoint()),
                listCoord.z.Value(listCoord.itPoint())
                );
    }
    structure_tensor.FindEigen();
    //SpatialAttributes spatialAttributes = structure_tensor.GetSpatialAttributes();
    //spatialAttributes.normalizeByLambda1();

    StructStructureTensor structure = structure_tensor.GetStruct();

    dimensionality.D1      = structure_tensor.GetDim().D1();
    dimensionality.D2      = structure_tensor.GetDim().D2();
    dimensionality.D3      = structure_tensor.GetDim().D3();
    dimensionality.entropy = structure_tensor.GetDim().Entropy();
    dimensionality.Dmax    = structure_tensor.GetDim().DMax();

    //dimensionality = spatialAttributes.dimensionality();

    const float r1 = 1/structure.sigma1;
    const float r2 = 1/structure.sigma2;
    const float r3 = 1/structure.sigma3;

    for(
            listCoord.itPoint() = listCoord.Begin();
            listCoord.itPoint() != listCoord.End();
            ++ listCoord.itPoint()
    )
    {
        //std::cout << listCoord.x.Value(listCoord.itPoint()) << "->";
        float x = listCoord.x.Value(listCoord.itPoint());
        float y = listCoord.y.Value(listCoord.itPoint());
        float z = listCoord.z.Value(listCoord.itPoint());

        structure.Center(x, y, z);

        structure.Rotation(x, y, z);

        listCoord.x.Value(listCoord.itPoint()) = r1 * x;
        listCoord.y.Value(listCoord.itPoint()) = r2 * y;
        listCoord.z.Value(listCoord.itPoint()) = r3 * z;

        //std::cout << listCoord.x.Value(listCoord.itPoint()) << " ";
    }
    std::cout << "Global PCA done" << std::endl;
}



