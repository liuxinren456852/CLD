#include "../LidarClassifStuff/NeighborhoodBounds.h"




const void NeighborhoodBounds::Display()
{
    std::cout <<" kMin : "<<KMin()<< std::endl;
    std::cout <<" kMax : "<<KMax()<< std::endl;
    std::cout <<" rMin : "<<RMin()<< std::endl;
    std::cout <<" rMax : "<<RMax()<< std::endl;
}

void FindNeighbor::operator()( const int k, const float r2 ){Test(k,r2);}
void FindNeighbor::Test( const int k, const float r2 )
{
    if(m_bounds.IsBetweenBounds2(k,r2))
    {
        m_k = k;
        m_r2 = r2;
        m_found = true;
    }
}
void FindNeighborMin::operator()( const int k, const float r2 ){if(k <= m_k)Test(k,r2); }
void FindNeighborMax::operator()( const int k, const float r2 ){if(k >= m_k)Test(k,r2);}

void FindBounds::operator()( const int k, const float r2 ){Test(k,r2);}
void FindBounds::Test( const int k, const float r2 )
{
    m_find_neighbor_min(k,r2);
    m_find_neighbor_max(k,r2);
}

const NeighborhoodBounds FindBounds::neighborhoodBounds()
{
    if(!m_find_neighbor_min.Found()&&!m_find_neighbor_max.Found())
    {
        return NeighborhoodBounds( m_bounds.KMin(), m_bounds.KMax(), m_bounds.RMin(), m_bounds.RMax() );
    }
    if(!m_find_neighbor_min.Found())
    {
        return NeighborhoodBounds(m_find_neighbor_max.K(),m_find_neighbor_max.K(),m_find_neighbor_max.R(),m_find_neighbor_max.R());
    }
    if(!m_find_neighbor_max.Found())
    {
        return NeighborhoodBounds(m_find_neighbor_min.K(),m_find_neighbor_min.K(),m_find_neighbor_min.R(),m_find_neighbor_min.R());
    }
    return NeighborhoodBounds(m_find_neighbor_min.K(),m_find_neighbor_max.K(),m_find_neighbor_min.R(),m_find_neighbor_max.R());
}

