# ComputeLocalDescriptors/lib_CLD CMakeLists
# adds lib_CLD (library containing the code for the Descriptors Computation)
# you should only modify this file when you add new files or dependencies to lib_CLD
# author: Stephane Guinard
# date: 22/09/2017

set(CLD src/LidarStructureTensor.cpp src/MyIndexation.cpp src/NeighborhoodBounds.cpp)

file(GLOB CLD_HEADERS ${PROJECT_SOURCE_DIR}/lib_CLD/*.h ${PROJECT_SOURCE_DIR}/lib_CLD/ANN/*.h ${PROJECT_SOURCE_DIR}/lib_CLD/LidarClassifStuff/*.h
    ${PROJECT_SOURCE_DIR}/lib_CLD/LidarClassifStuff/PCA/*.h ${PROJECT_SOURCE_DIR}/lib_CLD/LidarClassifStuff/List/*.h
    ${PROJECT_SOURCE_DIR}/lib_CLD/LidarClassifStuff/PList/*.h ${PROJECT_SOURCE_DIR}/lib_CLD/LidarClassifStuff/Struct/*.h)

add_library(CLD "SHARED" ${CLD} ${CLD_HEADERS})
target_link_libraries(CLD ${CLD_LIBRARIES})

if(CMAKE_VERBOSITY GREATER 0)
message(STATUS "Added target CLD (lib)")
endif(CMAKE_VERBOSITY GREATER 0)
