#ifndef NEIGHBORHOOD_BOUNDS_HPP
#define NEIGHBORHOOD_BOUNDS_HPP

#include "cmath"
#include <iostream>
#include <limits>
#include "KNearestNeighbor.h"

class NeighborhoodBounds
{
public :

    NeighborhoodBounds(
            const int k_min=0,
            const int k_max=std::numeric_limits< int >::max(),
            const float r_min=0,
            const float r_max=std::numeric_limits< float >::max()
            ):
    m_k_min(k_min),
    m_k_max(k_max),
    m_r_min(r_min),
    m_r_max(r_max),
    m_r2_min(r_min * r_min),
    m_r2_max(r_max *  r_max)
    {
    //display();
    }

    const void Display();

     bool IsBetweenBounds( const int k, const float r )   const {return IsBetweenKBounds(k) && IsBetweenRBounds(r);}
     bool IsBetweenBounds2( const int k, const float r2 ) const {return IsBetweenKBounds(k) && IsBetweenR2Bounds(r2);}

     bool IsBetweenKBounds( const int k)     const {return (k >= m_k_min) && ( k <= m_k_max);}
     bool IsBetweenRBounds(const float r )   const {return (r >= m_r_min) && ( r <= m_r_max);}
     bool IsBetweenR2Bounds(const float r2 ) const {return (r2 >= m_r2_min) && ( r2 <= m_r2_max);}

     int KMin()const{return m_k_min;}
     int KMax()const{return m_k_max;}

     float RMin()const{return m_r_min;}
     float RMax()const{return m_r_max;}

     float R2Min()const{return m_r2_min;}
     float R2Max()const{return m_r2_max;}

private :
        const int m_k_min;
        const int m_k_max;
        const float m_r_min;
        const float m_r_max;
        const float m_r2_min;
        const float m_r2_max;
};


/*
Neighbor est un voisin tel que
                k_min <= k <= k_max
                r_min <= r <= r_max
*/
struct FindNeighbor
{
    static const int K_DEFAULT_VALUE = 0;
    static constexpr float R2_DEFAULT_VALUE = 0;

    FindNeighbor(NeighborhoodBounds& bounds, const int k_default_value=K_DEFAULT_VALUE, const float r2_default_value=R2_DEFAULT_VALUE):
            m_bounds( bounds ),
            m_k( k_default_value ),
            m_r2( r2_default_value ),
            m_found(false)
            {}

    void operator()( const int k, const float r2 );
    void Test( const int k, const float r2 );

    const bool Found(){return m_found; }
    const int K(){return m_k;}
    const float R(){return std::sqrt(m_r2);}
    const float R2(){return m_r2;}

protected :
      NeighborhoodBounds& m_bounds;
      int m_k;
      float m_r2;
      bool m_found;
};

/*
minNeighbor est le voisin le-plus-proche tel que
                k_min <= k <= k_max
                r_min <= r <= r_max

si minNeighbor n est pas trouve,
                k = k_max
                r = r_min
                (on est alle jusqu a k_max sans depasser r_min)
*/
struct FindNeighborMin : public FindNeighbor
{
    FindNeighborMin(NeighborhoodBounds& bounds):FindNeighbor(bounds,bounds.KMax(),bounds.R2Min()){}
    void operator()( const int k, const float r2 );
};

/*
maxNeighbor est le voisin le-plus-loin tel que
                k_min <= k <= k_max
                r_min <= r <= r_max

si maxNeighbor n est pas trouve,
                k = k_min
                r = r_max
                (on est descendu jusqu a k_min sans descendre sous r_max)
*/
struct FindNeighborMax : public FindNeighbor
{
    FindNeighborMax(NeighborhoodBounds& bounds):FindNeighbor(bounds,bounds.KMin(),bounds.R2Max()){}
    void operator()( const int k, const float r2 );
};

struct FindBounds
{
        FindBounds(NeighborhoodBounds& bounds):m_bounds(bounds),m_find_neighbor_min(bounds),m_find_neighbor_max(bounds){}

        void operator()( const int k, const float r2 );
        void Test( const int k, const float r2 );

        const NeighborhoodBounds neighborhoodBounds();

        bool NeighborMinFound(){return m_find_neighbor_min.Found();}
        bool NeighborMaxFound(){return m_find_neighbor_max.Found();}

        private :
        NeighborhoodBounds& m_bounds;
        FindNeighborMin m_find_neighbor_min;
        FindNeighborMax m_find_neighbor_max;
};

#endif //NEIGHBORHOOD_BOUNDS_HPP
