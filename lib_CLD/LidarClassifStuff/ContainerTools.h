#pragma once

#include <string>
#include "LidarFormat/LidarDataContainer.h"

namespace Lf
{
/// charge les donnes du fichier \arg filename_xml dans le container \arg lidar_data_container
void LoadData(Lidar::LidarDataContainer& lidar_data_container,
              const std::string & filename_xml);

/// charge les donnes du fichier \arg filename_xml dans le container \arg lidar_data_container
void LoadData(Lidar::LidarDataContainer& lidar_data_container,
              Lidar::LidarCenteringTransfo& transfo,
              const std::string & filename_xml);

/// retourne un nom de fichier ayant le meme basename que \arg filename mais le suffixe \arg file_extension
std::string Name(const std::string & filename,
                 const std::string & file_extention);

std::string Name(const std::string& filename,
                 const std::string& prefixe,
                 const std::string& suffixe,
                 const std::string & file_extention);

/// enregistrement
//void Save(Lidar::LidarDataContainer& lidar_data_container, const std::string & filename,const cs::DataFormatType format);
void Save(Lidar::LidarDataContainer& lidar_data_container, Lidar::LidarCenteringTransfo& transfo,
          const std::string & filename,
          const cs::DataFormatType format = cs::DataFormatType::binary);

unsigned int Index(Lidar::LidarDataContainer& lidar_data_container, const Lidar::LidarDataContainer::iterator it);

bool CheckAttributeAndType(Lidar::LidarDataContainer& container,
                           std::string attrib_name,
                           Lidar::EnumLidarDataType expected_type,
                           std::string filename=""); // for display only

inline void Copy(const Lidar::LidarDataContainer::iterator first_in,
                 const Lidar::LidarDataContainer::iterator last_in,
                 const Lidar::LidarDataContainer::iterator first_out)
{
    Lidar::LidarDataContainer::iterator in  = first_in;
    Lidar::LidarDataContainer::iterator out = first_out;
    for(; in!=last_in; ++in, ++out) *out = *in;
}

} // namespace Lf

