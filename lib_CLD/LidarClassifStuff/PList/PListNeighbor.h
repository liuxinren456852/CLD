#ifndef __P_LIST_NEIGHBOR_H__
#define __P_LIST_NEIGHBOR_H__

#include "PList.h"
#include "../Struct/StructNeighbor.h"

class PListNeighbor : public PList
{
public :
TAttribute< int >k;
TAttribute< float >radius;

PListNeighbor() : PList(),
k("k",0),
radius("radius",0)
{
AddAttribute(&k);
AddAttribute(&radius);
}

StructNeighbor Get(const Lidar::LidarDataContainer::iterator it)
{
return StructNeighbor(
k.Value(it), 
radius.Value(it)
); 
}

void Get(StructNeighbor& struct_neighbor, const Lidar::LidarDataContainer::iterator it)
{
struct_neighbor.k = k.Value(it);
struct_neighbor.radius = radius.Value(it);
}

void Set(const Lidar::LidarDataContainer::iterator it, StructNeighbor& struct_neighbor)
{
k.Value(it) = struct_neighbor.k;
radius.Value(it) = struct_neighbor.radius;
}

void Set(
const Lidar::LidarDataContainer::iterator it, 
const int _k = 0,
const float _radius = 0
)
{
k.Value(it) = _k;
radius.Value(it) = _radius;
}

};
#endif //__P_LIST_NEIGHBOR_H__
