#ifndef __SYMMETRIC_MATRIX_H__
#define __SYMMETRIC_MATRIX_H__

#include <iostream>
#include <vector>

namespace Storage{
/*
        L'idee est qu'il y a (dim (dim+1))/2 valeurs au lieu de dim * dim car la matrice est symetrique.
        On stocke ces valeurs dans un tableau et on y accede avec l' operateur at(i,j)
*/

template< typename T>
class SymmetricMatrix
{

       int m_dim, m_size;
       T* mp_data;
       int* mp_shift;

    void Delete()
    {
                delete mp_data;
                delete mp_shift;
    }

    void Alloc()
    {
            mp_data = new T [m_size];
            mp_shift = new int [m_dim];
    }

    public :

                SymmetricMatrix(){}

                SymmetricMatrix(const int dim):
                m_dim(dim),
        m_size(dim * (dim + 1) / 2)
                {
            Alloc();
                        SetShifts();
                }

        ~SymmetricMatrix()
        {
            Delete();
        }

                SymmetricMatrix(const SymmetricMatrix& cs):
        m_dim(cs.m_dim),
        m_size(cs.m_size)
                {
                    Alloc();
                    CopyShifts(cs);
                    CopyMA(cs);
                }

                void operator = (const SymmetricMatrix& cs)
        {
            Copy(cs);
        }

        void Copy(const SymmetricMatrix& cs)
        {
            if(m_dim != cs.m_dim)
            {
                m_dim = cs.m_dim;
                m_size = cs.m_size;
                Delete();
                Alloc();
            }
                        CopyShifts(cs);
                        CopyMA(cs);
        }

                template<typename t_iterator>
                void Set(t_iterator iterator_in)
                {
                    Copy(iterator_in, mp_data, m_size);
                }

                template<typename t_iterator>
                void Get(t_iterator iterator_out)const
                {
                    Copy(mp_data, iterator_out, m_size);
                }

                template<typename t_binary_operator>
                void Transform(const SymmetricMatrix& cs, t_binary_operator binary_operator)
                {
                    T* p = mp_data;
                    T* p_cs = cs.mp_data;
                    for(int i=0; i!= m_size; ++i, ++p, ++p_cs) binary_operator(*p, *p_cs);
                }

                template<typename t_unary_operator>
                void Transform(t_unary_operator unary_operator)
                {
                    T* p = mp_data;
                    for(int i=0; i!= m_size; ++i, ++p) unary_operator(*p);
                }

                T At(const int i, const int j)const
                {
                    const int id = Index(i,j);
                    if(id >= m_size) std::cout << "SymmetricMatrix. ";
                    return *(mp_data + id);
                }

                T& At(const int i, const int j)
                {
                    const int id = Index(i,j);
                    if(id >= m_size) std::cout << "SymmetricMatrix. ";
                    return *(mp_data + id);
                }

                T  At(const int i)const{return At(i,i);}
                T& At(const int i)     {return At(i,i);}

                const int Index(const int i, const int j)const
                {
                    if(i <= j)  return mp_shift[i] + j - i;
                    else        return mp_shift[j] + i - j;
                }

                int Dim()const{return m_dim;}
        int Size()const{return m_size;}

    private :

            const int Shift(const int min_ij)
            {
                    return ( min_ij * ( 2 * m_dim + 1 - min_ij ) ) / 2;
            }

            void SetShifts()
            {
                    for(int i=0; i!= m_dim; ++i) mp_shift[i] = Shift(i);
            }

                template<typename t_iterator_in, typename t_iterator_out>
                void Copy(t_iterator_in in, t_iterator_out out, const int size)
        {
           for(int i=0; i!= size; ++i, ++in, ++out) *out = *in;
        }

        void CopyShifts(const SymmetricMatrix& cs)
        {
                    Copy(cs.mp_shift, mp_shift, m_dim);
        }

        void CopyMA(const SymmetricMatrix& cs)
        {
                        Copy(cs.mp_data, mp_data, m_size);
        }
};

}
#endif //__SYMMETRIC_MATRIX_H__
