/// @file   Sigmas.h
/// @author Jerome Demantke
/// @date   2012/11/28

#ifndef __SIGMAS_H__
#define __SIGMAS_H__

//namespace {

/// @brief
template<typename T>
class Sigmas
{
  T m_sigma1;
  T m_sigma2;
  T m_sigma3;

  static void Check(T& a1, T& a2, T& a3)
  {
      a3 = std::fabs(a3);
      a2 = std::max (a3, a2);
      a1 = std::max (a2, a1);
  }

  public :

  Sigmas():
  m_sigma1(0),
  m_sigma2(0),
  m_sigma3(0)
  {}

  Sigmas(T sigma1, T sigma2, T sigma3)
  {
      Check(sigma1, sigma2, sigma3);

      m_sigma1 = sigma1;
      m_sigma2 = sigma2;
      m_sigma3 = sigma3;
  }

  void SetFromLambdas(T lambda1, T lambda2, T lambda3)
  {
      Check(lambda1, lambda2, lambda3);

       m_sigma1 = sqrt(lambda1);
       m_sigma2 = sqrt(lambda2);
       m_sigma3 = sqrt(lambda3);
  }

  T Sigma1()const{return m_sigma1;}
  T Sigma2()const{return m_sigma2;}
  T Sigma3()const{return m_sigma3;}

  inline static T Bounded(T value)
  {
      if(value <= 0) return 0;
      if(value >= 1) return 1;
      return value;
  }

  T D1()const
  {
      if(m_sigma1 == 0) return 0;
      return Bounded((m_sigma1 - m_sigma2) / m_sigma1);
  }

  T D2()const
  {
      if(m_sigma1 == 0) return 0;
      return Bounded((m_sigma2 - m_sigma3) / m_sigma1);
  }

  T D3()const
  {
      if(m_sigma1 == 0) return 1;
      return Bounded(m_sigma3 / m_sigma1);
  }

  T E1()const
  {
      if(m_sigma1 == 0) return 0;
      return Bounded(m_sigma1 / (m_sigma1 + m_sigma2 + m_sigma3));
  }

  T E2()const
  {
      if(m_sigma1 == 0) return 0;
      return Bounded(m_sigma2 / (m_sigma1 + m_sigma2 + m_sigma3));
  }

  T E3()const
  {
      if(m_sigma1 == 0) return 1;
      return Bounded(m_sigma3 / (m_sigma1 + m_sigma2 + m_sigma3));
  }

  T Product()const
  {
      return m_sigma1 * m_sigma2 * m_sigma3;
  }

  T Omnivariance()const
  {
      static const T CONST= 1. / 3.;

      return (std::pow<T,T>(Product(), CONST));
  }

  T Volume()const
  {
      static const T CONST= 4.188790205; // CONST=4*PI/3

      return CONST * Product();
  }

  T Area()const
  {
      static const T PI= 3.141592654;

      return m_sigma1 * m_sigma2 * PI;
  }

};

//}//namespace

#endif //__SIGMAS_H__
