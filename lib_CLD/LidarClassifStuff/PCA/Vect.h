#ifndef __ALGO_VECT_H__
#define __ALGO_VECT_H__

namespace Algo
{

template< typename T, int size >
class Vect
{
   T m_[size];

   public:
                Vect(const T& defaultValue= 0){memset(defaultValue);}
                template<typename U> void operator =(const U& u){ memset(u);}
                void memset(const T& t){for(int i=0; i!=size; ++i) m_[i] = t;}

                template<typename U> Vect    (const Vect<U,size>& v){copy(v);}
                template<typename U> Vect    (const U* v)           {copy(v);}
                template<typename U> void operator =(const Vect<U,size>& v){copy(v);}
                template<typename U> void operator =(const U* v)           {copy(v);}

                template<typename U> void copy(const Vect<U,size>& v){for(int i=0; i!=size; ++i) m_[i] = v[i];}
                template<typename U> void copy(const U* v)                  {for(int i=0; i!=size; ++i) m_[i] = v[i];}

                template<typename U>void operator +=(const Vect<U,size>& v){for(int i=0; i!=size; ++i)m_[i] += v[i];}
                template<typename U>void operator -=(const Vect<U,size>& v){for(int i=0; i!=size; ++i)m_[i] -= v[i];}
                template<typename U>void operator *=(const Vect<U,size>& v){for(int i=0; i!=size; ++i)m_[i] *= v[i];}
                template<typename U>void operator /=(const Vect<U,size>& v){for(int i=0; i!=size; ++i)m_[i] /= v[i];}

                template<typename U> Vect operator + (const Vect<U,size>& v)const{Vect out(*this); out += v; return out;}
                template<typename U> Vect operator - (const Vect<U,size>& v)const{Vect out(*this); out -= v; return out;}
                template<typename U> Vect operator * (const Vect<U,size>& v)const{Vect out(*this); out *= v; return out;}
                template<typename U> Vect operator / (const Vect<U,size>& v)const{Vect out(*this); out /= v; return out;}

                template<typename U>void operator +=(const U* v){for(int i=0; i!=size; ++i)m_[i] += v[i];}
                template<typename U>void operator -=(const U* v){for(int i=0; i!=size; ++i)m_[i] -= v[i];}
                template<typename U>void operator *=(const U* v){for(int i=0; i!=size; ++i)m_[i] *= v[i];}
                template<typename U>void operator /=(const U* v){for(int i=0; i!=size; ++i)m_[i] /= v[i];}

                template<typename U> Vect operator + (const U* v)const{Vect out(*this); out += v; return out;}
                template<typename U> Vect operator - (const U* v)const{Vect out(*this); out -= v; return out;}
                template<typename U> Vect operator * (const U* v)const{Vect out(*this); out *= v; return out;}
                template<typename U> Vect operator / (const U* v)const{Vect out(*this); out /= v; return out;}

                template<typename U> Vect operator + (const U& u)const{Vect out(*this); out += u; return out;}
                template<typename U> Vect operator - (const U& u)const{Vect out(*this); out -= u; return out;}
                template<typename U> Vect operator * (const U& u)const{Vect out(*this); out *= u; return out;}
                template<typename U> Vect operator / (const U& u)const{Vect out(*this); out /= u; return out;}

                template<class U> void operator +=(const U& u){for(int i=0; i!=size; ++i)m_[i] += u;}
                template<class U> void operator -=(const U& u){for(int i=0; i!=size; ++i)m_[i] -= u;}
                template<class U> void operator *=(const U& u){for(int i=0; i!=size; ++i)m_[i] *= u;}
                template<class U> void operator /=(const U& u){for(int i=0; i!=size; ++i)m_[i] /= u;}

        T& operator [](const int dimension){return m_[dimension];}
                T& at(const int dimension){return m_[dimension];}

                T operator [](const int dimension)const{return m_[dimension];}
                T at(const int dimension)const{return m_[dimension];}

                T* begin(){return m_;}

                int Size(){return size;}

        T product(const int i, const int j)const{return m_[i] * m_[j];}

        T covariance(const Vect& _E, const int i, const int j)const
        {
                        return (m_[i] - _E.m_[i]) * (m_[j] - _E.m_[j]);
                }

        T SquaredDistance(const Vect& v)const
        {
           T squarred_distance = 0;

           for(int i=0; i!=size; ++i)
           {
               T delta = m_[i] - v.m_[i];

               squarred_distance += delta * delta;
           }

           return squarred_distance;
        }
};

template < typename T, int size >
Vect< T, size > operator - (const Vect< T, size >& v)
{
    return Vect< T, size >(v) * (-1);
}

template < typename U, typename T, int size > Vect< T, size >  operator + (const U& u, const Vect< T, size >& v) {return Vect< T, size >(v) + u;}
template < typename U, typename T, int size > Vect< T, size >  operator - (const U& u, const Vect< T, size >& v) {return Vect< T, size >(v) - u;}
template < typename U, typename T, int size > Vect< T, size >  operator * (const U& u, const Vect< T, size >& v) {return Vect< T, size >(v) * u;}
template < typename U, typename T, int size > Vect< T, size >  operator / (const U& u, const Vect< T, size >& v)
{
    Vect< T, size >out(v);
    for(int i=0; i!=size; ++i) out[i] = u / out[i];
    return out;
}

}

#endif //__ALGO_VECT_H__
