#ifndef COVARIANCE_STORAGE_HPP
#define COVARIANCE_STORAGE_HPP

#include "ostream"
#include "Vect.h"

/*
        Stocke les valeurs de variance et de covariance d'une distribution de "dim" dimensions
        L'idee est qu'il y a (dim (dim+1))/2 valeurs car la matrice est symetrique.
        On stocke ces valeurs dans un tableau et on y accede avec l' operateur at(i,j)
*/
template< typename T, int dim>
class CovarianceStorage
{
    public :

        CovarianceStorage():mv(0)
        {
            setShifts();
        }

        CovarianceStorage(const CovarianceStorage& cs):mv(cs.mv)
        {
            CopyShifts(cs.m_shift);
        }

        void Reset(){mv.memset(0);}

        const T Var(const int i)const{ return mv.at(index(i,i)); }

        T& Var(const int i){ return mv.at(index(i,i)); }

        T Cov(const int i, const int j)const{ return mv.at(index(i,j)); }

        T& Cov(const int i, const int j){ return mv.at(index(i,j)); }

        T at(const int i, const int j)const{return mv.at(index(i,j));}

        T& at(const int i, const int j){return mv.at(index(i,j));}

        T at(const int i)const{return mv.at(i);}

        T& at(const int i){return mv.at(i);}

        void Set(const Algo::Vect< T, dim >& v)
    {
                for(int i=0; i!=dim; ++i)
        {
                        for(int j=i; j!=dim; ++j)
            {
                                mv.at(i,j) = v.product(i,j);
            }
        }
    }

        const int index(const int i, const int j)const
        {
            if(i <= j)  return m_shift[i] + j - i;
            else        return m_shift[j] + i - j;
        }

        void operator /=(const T& t){mv /= t;}

    private :

        Algo::Vect< T,  (dim * (dim + 1)) / 2 > mv;
        int m_shift[dim];

        const int Shift(const int min_ij)
    {
                return ( min_ij * ( 2 * dim + 1 - min_ij ) ) / 2;
    }

    void setShifts()
    {
                for(int i=0; i!= dim; ++i) m_shift[i] = Shift(i);
        }

        void CopyShifts(const int* _shift)
        {
                for(int i=0; i!= dim; ++i) m_shift[i] = _shift[i];
        }
};

template< typename T >
class CovarianceStorage< T, 1 >
{

        public :

        CovarianceStorage():m_(0){}

        CovarianceStorage(const CovarianceStorage& cs):m_(cs.m_){}

        void Reset(){m_ = 0;}

        const T Var(const int i = 0)const{ return m_; }

        T& Var(const int i = 0){ return m_; }

        T Cov(const int i = 0, const int j = 0)const{ return m_; }

        T& Cov(const int i = 0, const int j = 0){ return m_; }

        T at(const int i = 0, const int j = 0)const{return m_;}

        T& at(const int i = 0, const int j = 0){return m_;}

        void Set(const Algo::Vect< T, 1 >& v)
        {
                m_ = v.at(0) * v.at(0);
        }

        const int index(const int i, const int j)const
        {
            return 0;
        }

        void operator /=(const T& t){m_ /= t;}

    private :

        T m_;
};

#endif //COVARIANCE_STORAGE_HPP
