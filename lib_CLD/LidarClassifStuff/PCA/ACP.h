#ifndef __ACP_H__
#define __ACP_H__

#include <ostream>
#include <vector>

#include <eigen3/Eigen/Eigenvalues>

//#include "Tensor3D.h"

// import most common Eigen types
using namespace Eigen;

namespace Stats
{
    template<int dimension, typename t_cov_matrix, typename _MatrixType>
    void FindCovarianceMatrix(const t_cov_matrix& cov_matrix, _MatrixType& D)
    {

        for(unsigned int i=0; i<dimension; i++)
        {
            D(i,i) = cov_matrix.Covariance(i,i);

            for(unsigned int j=0; j<i; j++)
            {
                D(i,j) = cov_matrix.Covariance(i,j);
                D(j,i) = D(i,j);
            }
        }
    }

    template<typename _MatrixType, typename _VectorType>
    void Solve(const _MatrixType& D, _MatrixType& eigenvectors, _VectorType& eigenvalues)
    {
        SelfAdjointEigenSolver<_MatrixType> saes = SelfAdjointEigenSolver<_MatrixType>(D);

        eigenvalues = saes.eigenvalues();
        eigenvectors = saes.eigenvectors();
    }

    template<int dimension, typename t_value, typename _MatrixType, typename _VectorType>
    void SetEigen(const _MatrixType& eigenvectors,
                  const _VectorType& eigenvalues,
                  std::vector< std::vector<t_value> >& vectors_out,
                  std::vector< t_value >&              values_out)
    {
        vectors_out.resize(dimension);
         values_out.resize(dimension);

        for(unsigned int col=0; col< dimension; col++)
        {
            vectors_out.at(col).resize(dimension);

            values_out.at(col) = eigenvalues(col);
            for(unsigned int raw=0; raw< dimension; raw++)
            {
                vectors_out.at(col).at(raw) = eigenvectors(raw,col);
            }
        }
    }

}

#endif /* __ACP_H__ */
