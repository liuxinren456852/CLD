#ifndef PRINCIPAL_COMPONENT_ANALYSIS_HPP_
#define PRINCIPAL_COMPONENT_ANALYSIS_HPP_

#include <ostream>
#include <cstdio>

#include "M2ACC.h"
#include "Weighted.h"

#include "ACP.h"

template< typename t_value, typename t_weight, int Dimension>
class PrincipalComponentAnalysis
{
private :

bool m_moments_gotten;
// Les moments (centroide + matrice de variance covariance)
// Contient le resultat final de l'accumulation.
M2<t_value, t_weight, Dimension> m_moments;  //appeler  moments()

// L'accumulateur de moments
M2Acc<t_value, t_weight, Dimension> m_M2Acc;

int m_nb_obs;

public:
    PrincipalComponentAnalysis():m_nb_obs(0){}

    void operator()(const t_value* obs, const t_value& weight=1)
    {
        push(obs,weight);
    }

    void push(const t_value* obs, const t_value& weight=1)
    {
        ++ m_nb_obs;
        m_moments_gotten = false;

        static WeightedVector< t_value, t_weight, Dimension> observation;

        for(unsigned int i=0; i<Dimension; i++)
        {
            observation.at(i) = obs[i];
        }
        observation.Weight() = weight;

        m_M2Acc.Push( observation );
    }

    //si Dimension==3
    void operator()(const t_value x, const t_value y, const t_value z, const t_value weight=1)
    {
        push(x,y,z,weight);
    }

    //si Dimension==3
    void push(const t_value x, const t_value y, const t_value z, const t_value weight=1)
    {
       ++ m_nb_obs;
       m_moments_gotten = false;

       static WeightedVector<t_value, t_weight, Dimension > observation;

                observation[0] = x;
                observation[1] = y;
                observation[2] = z;

        observation.Weight() = weight;

        m_M2Acc.Push( observation );
    }

    t_value SumWeight()const{return m_M2Acc.Weight();}


    template<typename _MatrixType, typename _VectorType>
    bool findEigen(_VectorType& eigenvalues, _MatrixType& eigenvectors)
    {
        if(isVoid())
        {
            std::cout << "PCA is void." << std::endl;
            return false;
        }
        else
        {
            _MatrixType covarianceMatrix;

            const M2<t_value, t_weight, Dimension>& m2(moments());

            Stats::FindCovarianceMatrix<Dimension, M2<t_value, t_weight, Dimension>, _MatrixType>(m2, covarianceMatrix);

            Stats::Solve<_MatrixType,_VectorType>(covarianceMatrix,eigenvectors,eigenvalues);

            //Stats::SetEigen<Dimension, t_value, MatrixXf, VectorXf> (acp.eigenvectors, acp.eigenvalues, eigenvectors, eigenvalues);

            return true;
        }
    }


    template<typename _VectorType>
    void getE( _VectorType& E)
    {
        for(unsigned int i=0; i<Dimension; i++)
        {
            E(i)=moments().E(i);
        }
    }

    M2<t_value, t_weight, Dimension>& moments()
    {
        if(! m_moments_gotten)
        {
            m_moments_gotten = true;

            m_M2Acc.Get(m_moments);
        }
        return m_moments;
    }

    void operator()(PrincipalComponentAnalysis& pca)
    {
        if(pca.m_nb_obs == 0) return;
        m_nb_obs += pca.nbObs();
        m_moments = M2<t_value, t_weight, Dimension>(moments(), pca.moments());
    }

    int nbObs()const{ return m_nb_obs;}

    bool isVoid()const{ return m_nb_obs == 0; }
};

template<typename _MatrixType> double e3x(const _MatrixType& eigen_vectors){return eigen_vectors(0,0);}
template<typename _MatrixType> double e2x(const _MatrixType& eigen_vectors){return eigen_vectors(0,1);}
template<typename _MatrixType> double e1x(const _MatrixType& eigen_vectors){return eigen_vectors(0,2);}
template<typename _MatrixType> double e3y(const _MatrixType& eigen_vectors){return eigen_vectors(1,0);}
template<typename _MatrixType> double e2y(const _MatrixType& eigen_vectors){return eigen_vectors(1,1);}
template<typename _MatrixType> double e1y(const _MatrixType& eigen_vectors){return eigen_vectors(1,2);}
template<typename _MatrixType> double e3z(const _MatrixType& eigen_vectors){return eigen_vectors(2,0);}
template<typename _MatrixType> double e2z(const _MatrixType& eigen_vectors){return eigen_vectors(2,1);}
template<typename _MatrixType> double e1z(const _MatrixType& eigen_vectors){return eigen_vectors(2,2);}

template<typename _MatrixType, typename t_point>
t_point e3(const _MatrixType& eigen_vectors)
{
    return t_point(e3x(eigen_vectors),
                   e3y(eigen_vectors),
                   e3z(eigen_vectors));
}

template<typename _MatrixType, typename t_point>
t_point e2(const _MatrixType& eigen_vectors)
{
    return t_point(e2x(eigen_vectors),
                   e2y(eigen_vectors),
                   e2z(eigen_vectors));
}

template<typename _MatrixType, typename t_point>
t_point e1(const _MatrixType& eigen_vectors)
{
    return t_point(e1x(eigen_vectors),
                   e1y(eigen_vectors),
                   e1z(eigen_vectors));
}

template<typename _VectorType> double lambda1(const _VectorType& eigen_values){return eigen_values(2);}
template<typename _VectorType> double lambda2(const _VectorType& eigen_values){return eigen_values(1);}
template<typename _VectorType> double lambda3(const _VectorType& eigen_values){return eigen_values(0);}

#endif /* PRINCIPAL_COMPONENT_ANALYSIS_HPP_ */
