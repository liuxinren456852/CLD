/// @file   Dimensionality.h
/// @author Jerome Demantke
/// @date   2012/11/29

#ifndef __DIMENSIONALITY_H__
#define __DIMENSIONALITY_H__

#include "Sigmas.h"
#include "../MinMax.h"

//namespace {

#define DEFAULT_DIMENSIONALITY 0.333333333333

/// @brief
template<typename T>
class Dimensionality
{


    T m_d1;
    T m_d2;

public :

    T D1()const{return m_d1;}
    T D2()const{return m_d2;}
    T D3()const{return 1. - m_d1 - m_d2;}

    Dimensionality(T d1=DEFAULT_DIMENSIONALITY,
                   T d2=DEFAULT_DIMENSIONALITY):
        m_d1(d1),
        m_d2(d2)
    {}

    Dimensionality(T sigma1, T sigma2, T sigma3)
    {
        const Sigmas<T> sigmas(sigma1, sigma2, sigma3);

        m_d1 = sigmas.D1();
        m_d2 = sigmas.D2();
    }

    Dimensionality(const Sigmas<T>& sigmas):
        m_d1(sigmas.D1()),
        m_d2(sigmas.D2())
    {}

    void Set(const Sigmas<T>& sigmas)
    {
        //std::cout<<"a";
        m_d1 = sigmas.D1();
        m_d2 = sigmas.D2();
    }

    /// @param normalize : pour que les valeurs soient entre 0 et 1
    T Entropy(bool normalize=true)const
    {
        static const T INVERSE_LN3 = 0.910239227; // = 1/ln3

        const T coef = normalize ? INVERSE_LN3 : 1.;

        return 	(
                    - std::log(std::pow(m_d1,m_d1))
                    - std::log(std::pow(m_d2,m_d2))
                    - std::log(std::pow(D3(),D3()))
                    )
                * coef;
    }

    T EigenEntropy(bool normalize=true)const
    {
        static const T INVERSE_LN3 = 0.910239227; // = 1/ln3

        const T coef = normalize ? INVERSE_LN3 : 1.;

        Sigmas<T> S;

        return 	(
                    - S.E1()*std::log(S.E1())
                    - S.E2()*std::log(S.E2())
                    - S.E3()*std::log(S.E3())
                    )
                * coef;
    }

    /// @return (entropie > ln2) ou (entropie normalisee > ln2/ln3)
    bool EntropyIsGreat(T entropy, bool normalized=true)const
    {
        static const T LN2      = 0.693147181; // ln2
        static const T LN2_NORM = 0.630929754; // ln2/ln3

        if(normalized) return entropy >= LN2_NORM;
        else           return entropy >= LN2;
    }

    int DMax()const
    {
        if(m_d1 > m_d2)
        {
            if(m_d1 > D3()) return 1; // d1 >  d3 && d1 >  d2
            else            return 3; // d3 >= d1 && d1 >  d2
        }
        else
        {
            if(m_d2 > D3()) return 2; // d2 >  d3 && d2 >= d1
            else            return 3; // d3 >= d2 && d2 >= d1
        }
    }

    static T ComputeDDD(const T a, const T b)
    {
        if(a == 0) return 0;
        const T sum = a + b;
        if(sum == 0) return 0;
        return a / sum;
    }

    static T DDD(const T d1, const T d2, const T d3)
    {
        Algo::Max<T, int> find_dmax;
        find_dmax(d1, 1);
        find_dmax(d2, 2);
        find_dmax(d3, 3);

        const int dmax = find_dmax.Id();

        if(dmax == 1)
        {
            return d1;
        }
        else if(dmax == 2)
        {
            return 1. + d2;
        }
        else
        {
            return 2. + d3;
        }
    }

    T DDD()const
    {
        return DDD(D1(), D2(), D3());
    }
};

//}//namespace

#endif //__DIMENSIONALITY_H__
