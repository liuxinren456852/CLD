#ifndef __SIGNATURE_H__
#define __SIGNATURE_H__

#include <cmath>

class Signature
{
	public :
	
		Signature():m_value(0){}
		
		//label == 1, 2 ou 3
		void operator()(const int label, const int k)
		{
                        const int a = log2(k);
			if(std::pow(2,a) != k) return;
			
                        int bin = 0;
                        if(label == 2) bin = 1;

                        m_value += bin * std::pow(2, a);
		} 
		
		int Get(){return m_value;}
		
	private :
		int m_value;
};

#endif //__SIGNATURE_H__
