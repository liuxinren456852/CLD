#ifndef __STRUCT_DIMENSIONALITY_H__
#define __STRUCT_DIMENSIONALITY_H__


struct StructDimensionality
{
public :
float D1;
float D2;
float D3;
float entropy;
int Dmax;
int Dmax_of_neighbors;
float similarity;
int k;
float radius;
int nb_labellings;

StructDimensionality():
D1(0),
D2(0),
D3(0),
entropy(1),
Dmax(0),
Dmax_of_neighbors(0),
similarity(0),
k(0),
radius(0),
nb_labellings(0)
{}

StructDimensionality(
float _D1,
float _D2,
float _D3,
float _entropy,
int _Dmax,
int _Dmax_of_neighbors,
float _similarity,
int _k,
float _radius,
int _nb_labellings
):
D1(_D1),
D2(_D2),
D3(_D3),
entropy(_entropy),
Dmax(_Dmax),
Dmax_of_neighbors(_Dmax_of_neighbors),
similarity(_similarity),
k(_k),
radius(_radius),
nb_labellings(_nb_labellings)
{}

};
#endif //__STRUCT_DIMENSIONALITY_H__
