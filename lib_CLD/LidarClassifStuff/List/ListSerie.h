#ifndef __LIST_SERIE_H__
#define __LIST_SERIE_H__

#include <vector>
#include <string.h>
#include <iostream>

#include "LidarFormat/LidarDataContainer.h"

#include "../PList/PList.h"

template<typename T>
class ListSerie : public PList
{
public:

    std::string basename;
    std::vector < TAttribute<T> > serie;

    /*
    void operator = (const ListSerie& ls)
                    {
        this->PList = ls.PList;
        basename = ls.basename;
        serie(ls.serie);
    }
    */

    static std::string name(const std::string basename, const int i)
    {
        std::ostringstream os;
        os << basename << "_" << i;
        return os.str();
    }

    static int GetSize(const std::string basename, Lidar::LidarDataContainer& lidarDataContainer)
    {
        std::vector< std::string > list;
        lidarDataContainer.getAttributeList(list);

        bool ZERO_FOUND=false;
        int i=0;

        for(std::vector< std::string >::iterator it=list.begin(); it!=list.end(); ++it)
        {
            std::string& attribute_name = *it;
            const std::string name_0 = name(basename, 0);

            if(!ZERO_FOUND)
            {
                if( attribute_name == name_0 ){ZERO_FOUND=true;}
            }
            if(ZERO_FOUND)
            {
                if( attribute_name == name(basename, i) )
                {
                    ++i;
                }
            }
        }

        std::cout << "nombre d'attributs de la serie: "<< i << std::endl;
        return i;
    }

    ListSerie():basename(""),PList(){}

    ListSerie(const std::string basename, Lidar::LidarDataContainer& lidarDataContainer):PList(),
    basename(basename)
    {
        Init(GetSize(basename, lidarDataContainer));
    }

    ListSerie(const std::string basename, const int size=0):PList(),
    basename(basename)
    {        
        Init(size);
    }

    void Init(const int size)
    {
        serie.reserve(size);
        for(int i=0; i<size; ++i)
        {
            serie.push_back( TAttribute<T>(name(i)) );
            AddAttribute(&serie.at(i));
        }
    }

    //le numero du dernier voisin = le nombre de voisins (point courant exclu)
    const int kMax(){ return serie.size()-1; }

    void Set(Lidar::LidarDataContainer& ldc, bool FORCE=false)
    {
        for(int i=0; i<serie.size(); ++i)
        {
            serie.at(i).SetDefaultValue(ldc,FORCE);
        }
    }
    
    std::string name(const int i)const
    {
        return name(basename, i);
    }
    
    std::string sizename()const
    {
        return basename + "_size";
    }

    void ComputeDimNumber(Lidar::LidarDataContainer::iterator it)
    {
        T value = 0;
        int pow3 = 1;
        for(int i=0; i!=serie.size(); ++i, pow3*=3)
        {
            const T c = serie.at(i).Value(it);
            const T chiffre = ((c<1)||(c>3)) ? 2 : (c-1);
            value += chiffre * pow3;
        }
    }
};

#endif // __LIST_SERIE_H__
