#ifndef __LIST_STRUCTURETENSOR_H__
#define __LIST_STRUCTURETENSOR_H__

#include "../PList/PListStructureTensor.h"

//#include "LiteGeom/LgPoint3.hpp"

class ListStructureTensor : public PListStructureTensor
{
public :

ListStructureTensor() : PListStructureTensor(){}

float Distance2ToMean(Lidar::LidarDataContainer::iterator it_mean, const float x, const float y, const float z)
{
    const float dx = gx.Value(it_mean) - x;
    const float dy = gy.Value(it_mean) - y;
    const float dz = gz.Value(it_mean) - z;

    return dx * dx + dy * dy + dz * dz;
}

float DistanceToMean(Lidar::LidarDataContainer::iterator it_mean, const float x, const float y, const float z)
{
    return std::sqrt(Distance2ToMean(it_mean, x, y, z));
}

Lg::Point3f N(Lidar::LidarDataContainer::iterator it)const
{
    return Lg::Point3f(eigenVector3x.Value(it),
                       eigenVector3y.Value(it),
                       eigenVector3z.Value(it));
}



};
#endif //__LIST_STRUCTURETENSOR_H__
