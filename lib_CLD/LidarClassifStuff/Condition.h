#ifndef __CONDITION_H__
#define __CONDITION_H__

namespace Lf
{
    struct IsTrue
    {
        template<typename T>
        bool operator()(T value)const{return value;}
    };

    template<typename T>
    struct EqualsValue
    {
        const T value;
        EqualsValue(T value):value(value){}
        bool operator()(T val)const{return val == value;}
    };

    template<typename T>
    struct LtValue
    {
        const T value;
        LtValue(T value):value(value){}
        bool operator()(T val)const{return val < value;}
    };

    template<typename T>
    struct GtValue
    {
        const T value;
        GtValue(T value):value(value){}
        bool operator()(T val)const{return val > value;}
    };

    template<typename T>
    struct LeqValue
    {
        const T value;
        LeqValue(T value):value(value){}
        bool operator()(T val)const{return val <= value;}
    };

    template<typename T>
    struct GeqValue
    {
        const T value;
        GeqValue(T value):value(value){}
        bool operator()(T val)const{return val >= value;}
    };
}

#endif //__CONDITION_H__
